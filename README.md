# MongoModule

### Installation

```
$ npm i @nestlab/mongo mongodb
$ npm i -D @types/mongodb
```

### Configuration

**Configuration for main module**
```typescript
import { MongoModule } from './mongo.module';

MongoModule.forRoot(
    [ // Connections
        {
            name: 'PRIMARY',
            uri: 'mongodb://localhost:27017/primary-database',
            reconnectionTimeout: 2000,
        },
        {
            name: 'SECONDARY',
            uri: 'mongodb://localhost:27017/secondary-database',
            reconnectionTimeout: 2000,
        },
    ], 
    [Entity1, Entity2], // Entities   
);

```

**Configuration for feature module**

```typescript
import { MongoModule } from './mongo.module';

MongoModule.forFeature([ // Entities
    Entity3,
    Entity4,
]); 

```

### Usage

**Entity declaration**

```typescript
@Entity('cars') // Collection name
export class Car {
  name: string;
}
```

**Inject connections**
```typescript
export class CarController {
    constructor(@InjectConnection('PRIMARY') primaryConnection: MongoConnection,
                @InjectConnection('SECONDARY') secondaryConnection: MongoConnection) {    
    } 
}
```

**Inject repository**
```typescript
export class CarController {
    constructor(@InjectRepository(Car) carRepositry: Repository<Car>,
                @InjectRepository(Entity) entityRepository: Repository<Entity>) {    
    } 
}
```

### Creation custom repositories
```typescript
@EntityRepository(Car)
export class CarRepository extends Repository<Car> {
    // Your implementation...
}
```

Injection the same as in default repositories.

Enjoy!

import { DynamicModule, Module, Provider, Type } from '@nestjs/common';
import { MongoConnection } from './mongo/services/mongo-connection';
import { Reflector } from '@nestjs/core';
import { Repository } from './mongo/services/repository';
import { IMongoModuleOptions } from './mongo/models/module-options';
import { getConnectionToken, getRepositoryToken } from './mongo/functions';
import { SchemaBuilder } from './schema/services/schema.builder';

@Module({})
export class MongoModule {
    private static defaultConnectionName: string;

    static forRoot(options: IMongoModuleOptions): DynamicModule {
        const connectionProviders = options.connections.map(option => {
            return {
                provide: getConnectionToken(option.name),
                useFactory: async () => new MongoConnection(option).connect(),
            };
        });

        if (options.connections.length) {
            this.defaultConnectionName = options.defaultConnection || options.connections[0]?.name || 'undefined';
        }

        const repositoryProviders = this.forFeature(options.entities || []);

        return {
            module: MongoModule,
            providers: [
                SchemaBuilder,
                ...connectionProviders,
                ...repositoryProviders,
            ],
            exports: [
                SchemaBuilder,
                ...connectionProviders,
                ...repositoryProviders,
            ],
        };
    }

    static forFeature(entities: Array<Type<object>> = []): Provider[] {
        return (entities || []).map(entity => this.getRepositoryProvider(entity, this.defaultConnectionName));
    }

    private static getRepositoryProvider(entity: new(...args: any[]) => object, defaultConnectionName: string): Provider {
        const reflector = new Reflector();
        const customRepository = reflector.get<new(...args: any[]) => object>(entity, entity);

        const collectionName = reflector.get<string>('collectionName', entity);
        const connectionName = reflector.get<string>('connectionName', entity);

        const token = getRepositoryToken(entity);

        if (customRepository) {
            return {
                provide: token,
                useFactory: (connection) => {
                    return new (customRepository.bind.apply(customRepository, [ null, connection, collectionName, entity]));
                },
                inject: [
                    getConnectionToken(connectionName || defaultConnectionName),
                ],
            };
        }

        return {
            provide: token,
            useFactory: (connection: MongoConnection) => {
                if (!collectionName) {
                    throw new Error(`Collection name for '${entity.name}' was not provided. Try use @Entity decorator.`);
                }

                return new Repository(connection, collectionName, entity);
            },
            inject: [
                getConnectionToken(connectionName || defaultConnectionName),
            ],
        };
    }
}

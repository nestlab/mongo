#!/usr/bin/env node

function main() {
	const child_process = require('child_process');
	const path = require('path');

	const child = child_process.spawnSync('ts-node', [path.resolve(__dirname, 'commands/schemas-generate.command.ts')], {
		stdio: "inherit",
		cwd: process.cwd(),
	});
	child.stdout
}

main();

import * as path from 'path';
import * as fs from 'fs';
import { CliOptions } from '../models/cli-options';

export function readCliOptions(): CliOptions {
	const packageFile = path.resolve(process.cwd(), 'package.json');

	if (!fs.existsSync(packageFile)) {
		throw new Error('package json not found');
	}

	const pack = require(packageFile);

	if (!pack?.mongo?.entitiesPath) {
		throw new Error('Add to package.json mongo.entitiesPath');
	}

	if (!pack?.mongo?.schemasPath) {
		throw new Error('Add to package.json mongo.schemasPath');
	}

	return {
		entitiesPath: pack.mongo.entitiesPath,
		schemasPath: pack.mongo.schemasPath,
	}
}

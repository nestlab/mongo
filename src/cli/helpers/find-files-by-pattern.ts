import * as glob from 'glob';

export async function findFilesByPattern(pattern: string): Promise<string[]> {
	return new Promise((resolve, reject) => {
		glob(pattern, (err, files) => {
			if (err) {
				reject(err);
				return;
			}

			resolve(files);
		});
	});
}

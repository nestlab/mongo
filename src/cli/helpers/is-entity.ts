import { Reflector } from '@nestjs/core';

export function isEntity(cls: new(...args) => any): boolean {
	return !!(new Reflector().get('collectionName', cls));
}

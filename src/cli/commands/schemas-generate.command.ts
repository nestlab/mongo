#!/usr/bin/env node
import * as fs from 'fs';
import * as path from 'path';
import { isEntity } from '../helpers/is-entity';
import { findFilesByPattern } from '../helpers/find-files-by-pattern';
import { readCliOptions } from '../helpers/read-options';
import { Reflector } from '@nestjs/core';
import { createValidationSchema } from '../../schema';

async function findEntities(entitiesPath: string): Promise<Array<new(...args) => any>> {
	const entities = [];
	const files = await findFilesByPattern(entitiesPath);
	files.forEach(file => {
		try {
			const module = require(path.resolve(process.cwd(), file));

			Object.keys(module || {}).forEach(key => {
				if (isEntity(module[key])) {
					entities.push(module[key]);
				}
			});

		} catch (e) {}
	});

	return entities;
}

async function main() {
	const options = readCliOptions();
	const entities = await findEntities(options.entitiesPath);
	const reflector = new Reflector();

	fs.mkdirSync(options.schemasPath, {recursive: true});

	for (const entity of entities) {
		const collection = reflector.get('collectionName', entity);

		const schema = createValidationSchema(entity);

		fs.writeFileSync(`${path.resolve(options.schemasPath, `${collection}.schema.json`)}`, JSON.stringify(schema, null, 4));
	}
}

main().catch(e => console.log(e.message, e.stack));

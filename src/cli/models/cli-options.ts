export interface CliOptions {
	entitiesPath: string;
	schemasPath: string;
}

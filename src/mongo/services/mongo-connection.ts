import { Collection, MongoClient } from 'mongodb';
import { Logger } from '@nestjs/common';
import { IMongoConnectionOptions } from '../models/mongo-connection-options';

export class MongoConnection {
    private readonly defaultReconnectionTimeout = 2000;
    private lastReconnectTimestamp = 0;
    private readonly logger: Logger;
    private client: MongoClient;
    private onConnectHandler: () => Promise<void> | void;

    constructor(private readonly settings: IMongoConnectionOptions) {
        this.logger = new Logger(`MongoDB '${this.settings.name}'`);
    }

    get name(): string {
        return this.settings.name;
    }

    set onConnect(handler: () => Promise<void> | void) {
        this.onConnectHandler = handler;
    }

    async connect(): Promise<this> {
        try {
            this.client = await MongoClient.connect(this.settings.uri, this.settings.options);
            this.logger.log(`Successful connected.`);
            this.client.on('close', async () => this.reconnect());

            return this;
        } catch (e) {
            this.logger.error(e);
            await this.awaitTimeout();

            return this.connect();
        }
    }

    getCollection<T>(colName: string): Collection<T> {
        if (!this.client) {
            this.logger.error(`Connection to '${this.settings.uri}' is not established.`);
        }
        return this.client.db().collection(colName);
    }

    getClient(): MongoClient {
        return this.client;
    }

    private async reconnect(): Promise<void> {
        const timestamp = new Date().getTime() / 1000;
        if (timestamp - this.lastReconnectTimestamp > 1) {
            this.lastReconnectTimestamp = timestamp;
            await this.awaitTimeout();
            await this.connect();
        }
    }

    private awaitTimeout(): Promise<void> {
        return new Promise<void>(resolve => setTimeout(resolve, this.settings.reconnectionTimeout || this.defaultReconnectionTimeout));
    }
}

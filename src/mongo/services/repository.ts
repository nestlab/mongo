import {
    Collection,
    CollectionInsertOneOptions,
    CommonOptions,
    Cursor,
    DeleteWriteOpResultObject,
    FilterQuery,
    FindOneOptions,
    MongoClient,
    UpdateOneOptions,
    UpdateQuery,
    UpdateWriteOpResult,
} from 'mongodb';
import { MongoConnection } from './mongo-connection';
import { NotFoundException, Type } from '@nestjs/common';
import { Pageable } from '../models/pageable';
import { MapFn, OptionalId, WithId } from '../types';

const DEFAULT_PAGE_SIZE = 20;

export class Repository<T> {
    public constructor(protected readonly connection: MongoConnection,
                       private readonly collectionName: string,
                       private readonly entity: Type<T>) {
    }

    get client(): MongoClient {
        return this.connection.getClient();
    }

    get collection(): Collection<T> {
        return this.connection.getCollection(this.collectionName);
    }

    async findAndMap<KeyType = string>(query: FilterQuery<T>,
                                       mapFn: MapFn<T, KeyType>): Promise<Map<KeyType, T>> {
        const list = await this.findAll(query);
        const map = new Map();

        list.forEach(item => {
            const key = mapFn(item);
            map.set(key, item);
        });

        return map;
    }

    findAll(query: FilterQuery<T> = {}): Promise<T[]> {
        return this.collection.find(query).toArray();
    }

    async findPageable(query?: FilterQuery<T>, pagination?: {page: number, size: number}, sort?: object): Promise<Pageable<T>> {
        const size = pagination ? pagination.size || DEFAULT_PAGE_SIZE : DEFAULT_PAGE_SIZE;
        const page = pagination ? pagination.page || 0 : 0;
        const cursor = this.collection.find<T>(query).sort(sort || {}).skip(size * page).limit(size);

        return {
            data: await cursor.toArray(),
            total: await cursor.count(),
        };
    }

    findByPage(query: FilterQuery<T>, pagination: {page: number, size: number}, sort: object = {}): Promise<T[]> {
        const size = pagination ? pagination.size || DEFAULT_PAGE_SIZE : DEFAULT_PAGE_SIZE;
        const page = pagination ? pagination.page || 0 : 0;
        return this.collection.find<T>(query).skip(size * page).sort(sort).limit(size).toArray();
    }

    find(query: FilterQuery<T>): Cursor<T> {
        return this.collection.find(query);
    }

    async findOne(filter: FilterQuery<T>, options?: FindOneOptions<T>): Promise<T | null> {
        const item = await this.collection.findOne(filter, options as FindOneOptions<any>);

        if (item) {
            return Object.assign(new this.entity(), item);
        }

        return null;
    }

    async findOneOrFail(filter: FilterQuery<T>, options?: FindOneOptions<T> & {notFoundMessage?: string}): Promise<T> {
        const item = await this.findOne(filter, options);

        if (!item) {
            throw new NotFoundException(options?.notFoundMessage);
        }

        return item;
    }

    async insertOne(docs: OptionalId<T>, options?: CollectionInsertOneOptions): Promise<WithId<T>> {
        const result = await this.collection.insertOne(docs as any, options);
        return result.ops.shift();
    }

    async insertMany(docs: Array<OptionalId<T>>, options?: CollectionInsertOneOptions): Promise<void> {
        await this.collection.insertMany(docs as any, options);
    }

    updateOne(filter: FilterQuery<T>, update: UpdateQuery<T> | Partial<T>, options?: UpdateOneOptions): Promise<UpdateWriteOpResult> {
        return this.collection.updateOne(filter, update, options);
    }

    updateMany(filter: FilterQuery<T>, update: UpdateQuery<T> | Partial<T>, options?: UpdateOneOptions): Promise<UpdateWriteOpResult> {
        return this.collection.updateMany(filter, update, options);
    }

    deleteOne(filter: FilterQuery<T>, options?: CommonOptions & { bypassDocumentValidation?: boolean }): Promise<DeleteWriteOpResultObject> {
        return this.collection.deleteOne(filter, options);
    }

    deleteMany(filter: FilterQuery<T>, options?: CommonOptions & { bypassDocumentValidation?: boolean }): Promise<DeleteWriteOpResultObject> {
        return this.collection.deleteMany(filter, options);
    }
}

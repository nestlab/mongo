import { ObjectId } from 'mongodb';

export type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;
export type OptionalId<T> = Omit<T, '_id'> & { _id?: any };
export type WithId<TSchema> = Omit<TSchema, '_id'> & { _id: ExtractIdType<TSchema> };
export type ExtractIdType<TSchema> =
  TSchema extends { _id: infer U } // user has defined a type for _id
    ? {} extends U ? Exclude<U, {}> :
    unknown extends U ? ObjectId : U
    : ObjectId; // user has not defined _id on schema

export type MapFn<EntityType, KeyType = string> = (e: EntityType) => KeyType;

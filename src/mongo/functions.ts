import { Type } from '@nestjs/common';
import { PREFIX_CONNECTION_PROVIDER } from '../provider.declarations';

export function getConnectionToken(name: string): string | Type<any> | Function {
    return `${PREFIX_CONNECTION_PROVIDER}:${name}`;
}

export function getRepositoryToken(entity: Type<any> | Function): string | Type<any> | Function {
    return entity;
}

import { SetMetadata, Type } from '@nestjs/common';

export const EntityRepository = (entity: Type<object>): ClassDecorator => {
    return target => {
        SetMetadata(entity, target)(entity);
    };
};

import { Inject } from '@nestjs/common';
import { PREFIX_CONNECTION_PROVIDER } from '../../provider.declarations';

export function InjectConnection(name: string): ParameterDecorator {
    const collectionToken = `${PREFIX_CONNECTION_PROVIDER}:${name}`;
    return (target: object, key: string | symbol, index?: number) => Inject(collectionToken)(target, key, index);
}

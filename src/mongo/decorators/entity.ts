import { SetMetadata } from '@nestjs/common';

export const Entity = (collectionName: string, connection?: string): ClassDecorator => {
    return target => {
        SetMetadata('collectionName', collectionName)(target);
        SetMetadata('connectionName', connection)(target);
    };
};

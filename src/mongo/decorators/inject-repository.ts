import { Inject, Type } from '@nestjs/common';

export function InjectRepository(entity: Type<object>): ParameterDecorator {
    return (target: object, key: string | symbol, index?: number) => Inject(entity)(target, key, index);
}

import { IMongoConnectionOptions } from './mongo-connection-options';
import { Type } from '@nestjs/common';

export interface IMongoModuleOptions {
    connections: IMongoConnectionOptions[];
    entities?: Array<Type<object>>;
    defaultConnection?: string;
}

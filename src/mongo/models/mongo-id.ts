import {ObjectId} from 'mongodb';

export class MongoId {
    _id?: string | ObjectId;

    constructor(id?: string | ObjectId) {
        if (id) {
            this._id = id;
        }
    }
}

import { MongoClientOptions } from 'mongodb';

export interface IMongoConnectionOptions {
    name?: string;
    uri: string;
    reconnectionTimeout?: number;
    options?: MongoClientOptions;
}

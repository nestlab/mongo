import { applyDecorators } from '@nestjs/common';
import { createPropertyDecorator } from '../helpers/create-property-decorator';
import { BsonType } from '../types';

export namespace Schema {

	export const IsNullable = (): PropertyDecorator => createPropertyDecorator('nullable', true);

	export const Type = (...type: BsonType[]): PropertyDecorator => createPropertyDecorator('type', type);

	export const ArrayOf = (...type: BsonType[]): PropertyDecorator => createPropertyDecorator('arrayOf', type);

	export const Enum = (enumType: object | number[] | string[]): PropertyDecorator => createPropertyDecorator('enum', enumType);

	export const Min = (min: number): PropertyDecorator => createPropertyDecorator('min', min);

	export const Max = (max: number): PropertyDecorator => createPropertyDecorator('max', max);

	export const MinLength = (length: number): PropertyDecorator => createPropertyDecorator('minLength', length);

	export const MaxLength = (length: number): PropertyDecorator => createPropertyDecorator('maxLength', length);

	export const MinItems = (count: number): PropertyDecorator => createPropertyDecorator('minItems', count);

	export const MaxItems = (count: number): PropertyDecorator => createPropertyDecorator('maxItems', count);

	export const Pattern = (pattern: string): PropertyDecorator => createPropertyDecorator('pattern', pattern);

	export const Description = (description: string): PropertyDecorator => createPropertyDecorator('description', description);

	export const Embeddable = (type: new() => any, options?: { additionalProperties?: boolean, forEach?: boolean }): PropertyDecorator => applyDecorators(
		createPropertyDecorator('embeddableType', type),
		createPropertyDecorator('embeddableOptions', options),
	);
}

export const createPropertyDecorator = (metaKey: string, metaValue: any): PropertyDecorator => {
	return (target, propertyKey) => {
		const declarationKey = `schema:declare:${propertyKey.toString()}`;
		if (!Reflect.hasMetadata(declarationKey, target)) {
			Reflect.defineMetadata(declarationKey, propertyKey.toString(), target);
		}
		Reflect.defineMetadata(`schema:prop:${metaKey}`, metaValue, target, propertyKey);
	}
};

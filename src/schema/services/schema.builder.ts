import { JsonPropertySchema } from '../models/json-property-schema';
import { SchemaMetadata } from '../models/schema-metadata';
import { PropertyMetadata } from '../models/property-metadata';
import { BsonType } from '../types';

export class SchemaBuilder {
	static build(cls: new(...args) => any): object {
		return {
			$jsonSchema: this.buildForClass(cls),
		};
	}

	static buildForClass(cls: new(...args) => any): {bsonType: BsonType, required: string[], properties: {[prop: string]: JsonPropertySchema}} {
		const metadata = this.exploreMetadata(cls);

		const properties: {[prop: string]: JsonPropertySchema} = {};

		Object.keys(metadata).forEach(propName => {
			properties[propName] = this.buildProperty(metadata[propName]);
		});

		return {
			bsonType: 'object',
			required: this.findRequiredProps(metadata),
			properties,
		};
	}

	static exploreMetadata(cls: new(...args) => any): SchemaMetadata {
		const props: string[] = this.explorePropertyDeclarations(cls);
		const meta: SchemaMetadata = {};

		props.forEach(propName => {
			meta[propName] = {};

			const assignProperty = (name: string) => {
				const value = Reflect.getMetadata(this.getPropMetaKey(name), cls.prototype, propName);

				if (value !== undefined) {
					meta[propName][name] = value;
				}
			};

			assignProperty('type');
			assignProperty('nullable');
			assignProperty('arrayOf');
			assignProperty('min');
			assignProperty('max');
			assignProperty('minLength');
			assignProperty('maxLength');
			assignProperty('minItems');
			assignProperty('maxItems');
			assignProperty('pattern');
			assignProperty('embeddableType');
			assignProperty('embeddableOptions');

			const enumType = Reflect.getMetadata(this.getPropMetaKey('enum'), cls.prototype, propName);

			if (enumType) {
				meta[propName].enum = Array.isArray(enumType) ? enumType : Object.values(enumType);
			}
		});

		return meta;
	}

	static explorePropertyDeclarations(cls: new(...args) => any): string[] {
		return (Reflect.getOwnMetadataKeys(cls.prototype) || [])
			.filter(i => !!i)
			.filter(i => i.toString().indexOf('schema:declare:') === 0)
			.map(i => i.toString().split(':').pop());
	}

	static buildProperty(propMeta: PropertyMetadata): JsonPropertySchema {
		const propSchema: JsonPropertySchema = {};

		const assignPropertiesToSchema = (schema: JsonPropertySchema) => {
			const types = propMeta.type || propMeta.arrayOf;
			if (types) {
				schema.bsonType = types.length === 1 ? types[0] : types;
			}

			if (propMeta.min !== undefined) {
				schema.minimum = propMeta.min;
			}

			if (propMeta.max !== undefined) {
				schema.maximum = propMeta.max;
			}

			if (propMeta.minLength !== undefined) {
				schema.minLength = propMeta.minLength;
			}

			if (propMeta.maxLength !== undefined) {
				schema.maxLength = propMeta.maxLength;
			}

			if (propMeta.pattern) {
				schema.pattern = propMeta.pattern;
			}

			if (propMeta.enum) {
				schema.enum = propMeta.enum;
			}
		};

		const assignArrayPropertiesToSchema = (schema: JsonPropertySchema) => {
			if (propMeta.minItems !== undefined) {
				schema.minItems = propMeta.minItems;
			}

			if (propMeta.maxItems !== undefined) {
				schema.maxItems = propMeta.maxItems;
			}
		};

		propSchema.description = propMeta.description || this.buildDefaultDescription(propMeta);

		if (propMeta.embeddableType) {
			const primaryType: BsonType = propMeta.embeddableOptions?.forEach ? 'array' : 'object';
			propSchema.bsonType = propMeta.nullable ? [primaryType, 'null'] : primaryType;

			const {required, properties} = this.buildForClass(propMeta.embeddableType);

			propSchema.required = required;

			primaryType === 'object'
				? propSchema.properties = properties
				: propSchema.items = properties;

			propSchema.additionalProperties = propMeta.embeddableOptions?.additionalProperties !== undefined
				? propMeta.embeddableOptions.additionalProperties
				: true;

			assignPropertiesToSchema(propSchema);

			return propSchema;
		}

		if (propMeta.arrayOf) {
			propSchema.items = {};
			assignArrayPropertiesToSchema(propSchema);
			assignPropertiesToSchema(propSchema.items);

			return propSchema;
		}

		assignPropertiesToSchema(propSchema);

		return propSchema;
	}

	static buildDefaultDescription(propMeta: PropertyMetadata): string {
		const list = [];

		if (!propMeta.nullable) {
			list.push('is required');
		}

		if (propMeta.type) {
			list.push(`must be a ${propMeta.type}`);
		}

		if (propMeta.enum) {
			list.push(`must be an enum [${propMeta.enum.join(', ')}]`);
		}

		if (propMeta.min) {
			list.push(`must be greater or equal than ${propMeta.min}`);
		}

		if (propMeta.min) {
			list.push(`must be less or equal than ${propMeta.max}`);
		}

		if (propMeta.minLength) {
			list.push(`length must be greater or equal than ${propMeta.minLength}`);
		}

		if (propMeta.minLength) {
			list.push(`length must be less or equal than ${propMeta.maxLength}`);
		}

		if (propMeta.minItems) {
			list.push(`items counts must be greater or equal than ${propMeta.minItems}`);
		}

		if (propMeta.maxItems) {
			list.push(`items counts must be less or equal than ${propMeta.maxItems}`);
		}

		if (propMeta.pattern) {
			list.push(`must be match the pattern ${propMeta.pattern}`);
		}

		return list.join(', ');
	}

	static findRequiredProps(meta: SchemaMetadata): string[] {
		return Object.keys(meta).filter(key => meta[key].nullable !== true);
	}

	static getPropMetaKey(metaKey: string): string {
		return `schema:prop:${metaKey}`;
	}
}

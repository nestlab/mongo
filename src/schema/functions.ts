import { SchemaBuilder } from './services/schema.builder';

export function createValidationSchema(entity: new(...args) => any): object {
	return SchemaBuilder.build(entity);
}

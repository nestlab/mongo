import { PropertyMetadata } from './property-metadata';

export interface SchemaMetadata {
	[key: string]: PropertyMetadata;
}

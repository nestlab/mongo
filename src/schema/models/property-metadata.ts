import { BsonType } from '../types';

export interface PropertyMetadata {
	nullable?: boolean;
	type?: BsonType[];
	arrayOf?: BsonType[];
	enum?: number[] | string[];
	min?: number;
	max?: number;
	minLength?: number;
	maxLength?: number;
	minItems?: number;
	maxItems?: number;
	pattern?: string;
	embeddableType?: new(...args) => any;
	embeddableOptions?: {additionalProperties: boolean, forEach?: boolean};
	description?: string;
}

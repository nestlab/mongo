import { BsonType } from '../types';

export interface JsonPropertySchema {
	bsonType?: BsonType | BsonType[];
	arrayOf?: BsonType | BsonType[];
	enum?: string[] | number[];
	minimum?: number;
	maximum?: number;
	minLength?: number;
	maxLength?: number;
	minItems?: number;
	maxItems?: number;
	pattern?: string;
	description?: string;
	properties?: {[prop: string]: JsonPropertySchema};
	items?: {[prop: string]: JsonPropertySchema};
	required?: string[],
	additionalProperties?: boolean;
}

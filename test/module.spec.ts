import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import {
    createValidationSchema,
    getConnectionToken,
    getRepositoryToken,
    MongoConnection,
    MongoModule,
    Repository
} from '../src';
import { config } from 'dotenv';
import { User } from './entities/user';
import { Log } from './entities/log';
import { UserRepository } from './repositories/user.repository';

describe('MongoModule', () => {
    let app: INestApplication;

    const defaultConnectionName = 'DEFAULT';
    const logsConnectionName = 'LOGS';

    beforeAll(async () => {
        // if (process.env.NODE_ENV !== 'test') {
            config({path: '.env'});
        // }

        const reconnectionTimeout = +process.env.MONGO_RECONNECTION_TIMEOUT;
        const options = {
            useUnifiedTopology: true,
            useNewUrlParser: true
        };

        const testingModule = await Test.createTestingModule({
            imports: [
                MongoModule.forRoot({
                    connections: [
                        {
                            name: defaultConnectionName,
                            uri: process.env.MONGO_DEFAULT_URL,
                            reconnectionTimeout,
                            options,
                        },
                        {
                            name: logsConnectionName,
                            uri: process.env.MONGO_LOGS_URL,
                            reconnectionTimeout,
                            options,
                        },
                    ],
                    entities: [
                        User,
                        Log,
                    ],
                }),
            ],
        }).compile();

        app = testingModule.createNestApplication();
    });

    test('Dependency injection', () => {
        testInjectConnection(defaultConnectionName);
        testInjectConnection(logsConnectionName);

        testRepository(User, 'users', UserRepository);
        testRepository(Log, 'logs');
    });

    test('Insert and find',  async () => {
        const userRepository = app.get<UserRepository>(getRepositoryToken(User));

        await userRepository.insertMany([
            new User('firstName1', 'lastName1', 'username1', 'password_hash'),
            new User('firstName2', 'lastName2', 'username2', 'password_hash'),
            new User('firstName3', 'lastName3', 'username3', 'password_hash'),
        ]);

        const user = await userRepository.getByUsername('username1');

        expect(user).toBeInstanceOf(User);
    });

    test('Decorators', () => {
        const schema = createValidationSchema(User);

        console.log(JSON.stringify(schema, null, 4));

        expect(schema).toBeDefined();
    });

    const testInjectConnection = (name: string) => {
        const connection = app.get<MongoConnection>(getConnectionToken(name));
        expect(connection).toBeInstanceOf(MongoConnection);
        expect(connection.name).toBe(name);
    };

    const testRepository = (entity: Function, collectionName: string, repositoryType: Function = Repository) => {
        const repository = app.get<Repository<any>>(getRepositoryToken(entity));
        expect(repository).toBeInstanceOf(repositoryType);
        expect(repository.collection.collectionName).toBe(collectionName);
    };
});

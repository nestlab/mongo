import { EntityRepository, Repository } from '../../src';
import { User } from '../entities/user';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
	getByUsername(username: string): Promise<User> {
		return this.findOne({username});
	}
}

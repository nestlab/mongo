import { Entity, MongoId, Schema } from '../../src';

export enum Role {
	Admin = 'ADMIN',
	Manager = 'MANAGER',
	User = 'USER',
}

export class UserSettings {
	@Schema.Type('bool')
	use2FA: boolean;

	@Schema.Type('bool')
	enabledNotifications: boolean;
}

@Entity('users')
export class User extends MongoId {
	@Schema.Type('string')
	@Schema.MinLength(2)
	@Schema.MaxLength(32)
	firstName: string;

	@Schema.Type('string')
	@Schema.MinLength(2)
	@Schema.MaxLength(32)
	lastName: string;

	@Schema.Type('string')
	@Schema.MinLength(2)
	@Schema.MaxLength(32)
	@Schema.Pattern('[A-Z]')
	@Schema.Description('Incorrect username')
	username: string;

	@Schema.Type('string')
	@Schema.MinLength(8)
	@Schema.MaxLength(128)
	password: string;

	@Schema.Type('double')
	@Schema.Min(0)
	@Schema.Max(99999999)
	balance: number;

	@Schema.Enum(Role)
	role: Role;

	@Schema.ArrayOf('string')
	@Schema.MinItems(1)
	@Schema.MaxItems(10)
	tags: string[];

	@Schema.Embeddable(UserSettings, {additionalProperties: false, forEach: true})
	settings: UserSettings[];

	@Schema.IsNullable()
	@Schema.Type('date')
	updatedAt?: Date;

	@Schema.Type('date')
	createdAt: Date;

	constructor(firstName: string, lastName: string, username: string, password: string) {
		super();

		const initial: Partial<User> = {
			firstName,
			lastName,
			username,
			password,
			createdAt: new Date(),
		};

		Object.assign(this, initial);
	}
}

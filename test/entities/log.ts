import { Entity, MongoId, Schema } from '../../src';

@Entity('logs', 'LOGS')
export class Log extends MongoId {
	@Schema.Type('string')
	message: string;

	@Schema.Type('date')
	createdAt: Date;

	constructor(message: string) {
		super();

		const initial: Partial<Log> = {
			message,
			createdAt: new Date(),
		};

		Object.assign(this, initial);
	}
}
